<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;


class UserController extends Controller
{

    public function __construct()
    {
        parent:: __construct();
    }

    public function showAll() {
       
        $data = $this->auth->listUsers();
        $users = array();

        foreach ($data as $value) $users[] = $value;

        $response = !empty($users) 
            ? response()->json(['status' => 'success', 'data' => $users], 200) 
            : response()->json(['status' => 'failure', 'message' => 'No users found in database'], 500) ;

        return $response;

    }

    public function showSingle(Request $request) {
        
        $email = $request->route('email');
        if (empty($email) && !isset($email)) return false; // Retorne falso caso 'email' esteja vazio
        
        $users = $this->auth->getUserByEmail($email);

        $response = !empty($users) 
            ? response()->json(['status' => 'success', 'data' => $users], 200) 
            : response()->json(['status' => 'success', 'message' => 'User not found'], 500); 
            
        return $response;
    }

    public function insert(Request $request) {

        $data = $this->validate($request, $this->usersInsertRules);
        $users = $this->auth->listUsers();
        
        // Verifica se o email informado já existe
        foreach ($users as $user) {
            if (strcmp($data['email'], $user->email) == 0) {
                return response()->json(['status' => 'failure', 'message' => 'Given email already exists'], 500);
            }
        }
    
        $response = $this->auth->createUser($data) 
            ? response()->json(['status' => 'success', 'message' => 'User created successfully']) 
            : response()->json(['status' => 'failure', 'message' => 'User could not be created']);
            
        return $response;

    }

    public function update(Request $request) {

        $email = $request->route('email');
        if (empty($email) && !isset($email)) return false; // Retorne falso caso 'email' esteja vazio

        $data = $this->validate($request, $this->usersUpdateRules);
        $data = $request->only(['email', 'password', 'displayName']);
      
        $users = $this->auth->listUsers();

        // Verifica se o email informado já existe
        $user = $this->auth->getUserByEmail($email);
    
        if (isset($data['email']) && !empty($data['email'])) {
            foreach ($users as $user) {
                if (strcmp($data['email'], $user->email) == 0) {
                    return response()->json(['status' => 'failure', 'message' => 'Given email already exists'], 500);
                }
            }
        }
        
        foreach ($data as $key => $value) 
            if (!empty($value)) $update[$key] = $value;
        
        if ($this->auth->updateUser($user->uid, $update))
            $response = response()->json(['status' => 'success', 'message' => 'User updated successfully'], 200);
        else 
            $response = response()->json(['status' => 'failure', 'message' => 'Error updating user'], 500);
        
        return $response;
        
    }
    
    public function delete(Request $request) {
        
        $email = $request->route('email');
        if (empty($email) && !isset($email)) return false; // Retorne falso caso 'email' esteja vazio
    
        $user = $this->auth->getUserByEmail($email);
        $this->auth->deleteUser($user->uid);

        $response = response()->json(['status' => 'success', 'message' => 'User removed successfully'], 200);
        return $response;
    }

}