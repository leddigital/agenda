<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public $table = 'schedule';

    public function showAll(){
        
        $data = $this->database->getReference($this->table);
        $register = $data->getSnapshot()->getValue();
        $response = isset($register) && !empty($register) ? response()->json(['status' => 'success', 'data' => $register], 200) : response()->json(['status '=> 'failure','message' => 'Empty database'], 200);
        
        return $response;
        
    }

    public function showSingle(Request $request){
        
        $id = $request->route('id');
        $reference = $this->database->getReference($this->table)->getChild($id);
        $register = $reference->getSnapshot()->getValue();

        $response = isset($register) && !empty($register) ? response()->json(['status' => 'success', 'data' => $register]) : response()->json(['status' => 'failure', 'message' => 'Received identificator value was not found in database'], 200) ;

        return $response;

    }

    public function insert(Request $request){

        // Valida os dados de entrada
        $data = $this->validate($request, $this->scheduleInsertRules);

        // Verifica se usuário não tem evento no mesmo período informado
        $events = $this->database->getReference($this->table)->getValue();
        
        foreach ($events as $event) {
            if(strcmp($event['created_by'], $_SERVER['PHP_AUTH_USER']) == 0) {
               
                // Verifica se o horário informado é conflitante com o evento
                if (((strtotime($data['start_date']) >= strtotime($event['start_date'])) 
                    && (strtotime($data['start_date']) <= strtotime($event['end_date']))) 
                    || ((strtotime($data['end_date']) >= strtotime($event['start_date'])) 
                    && (strtotime($data['end_date']) <= strtotime($event['end_date'])))) {

                    return response()->json(['status' => 'error', 'message' => 'Conflict timestamp: user is not available in this timestamp'], 500);
               }
            } 
        }

        // Verifica se o usuário está autenticado e recebe o valor do email
        if(isset($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_USER']))
            $data['created_by'] = $_SERVER['PHP_AUTH_USER'];
        else 
            return response()->json(['status' => 'error', 'message' => 'No user is authenticated']);

        // Recebe o valor atual para inserir no campo 'created at'
        $data['created_at'] = date('Y-m-d h:i:s', time());

        // Se o 'type' do envento for 'Interno' ou 'Vídeoconferência', Agência LED será o endereço
        $data['location'] = strcmp($data['type'], 'Interna') == 0 || strcmp($data['type'], 'Vídeoconferência') == 0
            ?   'R. Santo André, 185 - Jardim Europa, São José do Rio Preto - SP, 15014-490' 
            :   $data['location'];
 
        // Registra os dados no banco
        $reference = $this->database->getReference($this->table)->push($data)->getKey();
        $response = isset($reference) && !empty($reference) ? response()->json(['status' => 'success', 'message' => 'Data has been written in database'], 200) : response()->json(['status' => 'failure', 'message' => 'Data has not been written in database'], 200);
        return $response;

    }

    public function filter(Request $request){

        $filtered_data = array();
        $reference = $this->database->getReference($this->table)->getSnapshot()->getValue();

        // Checa se os valores 'i' e 'f' estão sendo passado pela url e os valida
        if ($request->has(['i', 'f']))  {
            $data = $this->getValues($request, $type='date');

            foreach ($reference as $key => $value) {

               if (strtotime($data['i']) <= strtotime($value['start_date']) 
                    && strtotime($data['f']) >= strtotime($value['end_date'])) {
                    $filtered_data[] = $value;
                }

            } 
        }
        // Checa se o valor 'user' está sendo passado pela url e o valida
        else if ($request->has(['user'])) { 
            $data = $this->getValues($request, $type='user');

            // Compara o valor 'user' com o campo 'created_by' e armazena o registro na response
            foreach ($reference as $key => $value) {
                
                if (strcmp($data['user'], $value['created_by']) == 0) {
                    $filtered_data[] = $value;
                } 
                // Percorre o campo 'tagged_emails' comparando com o valor 'user' e armazena o registro na response
                foreach ($value['tagged_emails'] as $number => $email) {
                        
                    if (strcmp($email, $data['user']) == 0) {
                        $filtered_data[] = $value;
                    }
                }
            }
        }
        // Caso os valores passados sejam diferentos dos acima, retorne erro
        else {
            $response = response()->json(['status' => 'failure', 'message' => 'Wrong url parameters sent'], 200);
        }
        
        $response = !empty($filtered_data) ? response()->json(['status' => 'success', 'data' => $filtered_data], 200) : response()->json(['status' => 'failure', 'message' => 'No data found with specific variable'], 200);  
        return $response;

    }

    public function update(Request $request){

        /*
        ** Verificar o update do campo 'tagged_emails'
        */

        $data = $this->validate($request, $this->scheduleUpdateRules);
        $id = $request->route('id');

        foreach ($data as $key => $value) {
            if (!empty($value) && $value!= "" && $key!="tagged_emails") {
                $reference = $this->database->getReference($this->table)->getChild($id)->update([$key => $value]);
            }
        }

        $response = isset($reference) && !empty($reference) ? response()->json(['status' => 'success', 'message' => 'Register updated successfully']) : response()->json(['status' => 'failure', 'message' => 'Data could not be updated'], 200);        
        return $response;

    }

    public function delete(Request $request){

        $id = $request->route('id');
        $reference = $this->database->getReference($this->table)->getChild($id);
        $response = $reference->remove() ? response()->json(['status' => 'success', 'message' => 'Data has been deleted from database'], 200) : response()->json(['status' => 'failure', 'message' => 'Received identificator value was not found in database'], 200);
        
        return $response;

    }

}
