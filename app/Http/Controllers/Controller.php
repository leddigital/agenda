<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Database;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Exception\Auth\EmailExists as FirebaseEmailExists;

class Controller extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Validações de entrada de dados
    |--------------------------------------------------------------------------
    */

    public $scheduleInsertRules = [
        'title' => 'required|max:255',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after_or_equal:start_date',
        'description' => 'string',
        'location' => 'required_if:type,==,Externa|string',
        'type' => 'required|string|in:Interna,Externa,Vídeoconferência',
        'tagged_emails' => 'required'
    ];

    public $scheduleUpdateRules = [
        'title' => 'max:255',
        'start_date' => 'date',
        'end_date' => 'date|after_or_equal:start_date',
        'created_by' => '',
        'description' => '',
        'tagged_emails' => '',
        'type' => 'string',
        'location' => 'string',
        'tagged_emails[]' => 'email'
    ];

    public $usersInsertRules = [
        'email' => 'bail|required|email',
        'password' => 'bail|required|confirmed',
        'displayName' => 'bail|required|string|min:3'
    ];

    public $usersUpdateRules = [
        'email' => 'email',
        'password' => 'confirmed',
        'displayName' => 'string|min:3'
    ];

    /*
    |--------------------------------------------------------------------------
    | Fim das regras de validações
    |--------------------------------------------------------------------------
    */
    
    public $database;
    public $auth;

    public function __construct() {

        $this->middleware('auth');

        $serviceAccount = "../agenda-fe70f-5f69c9e3350d.json";

        $factory = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://agenda-fe70f.firebaseio.com/');

        $this->database = $factory->createDatabase();
        $this->auth = $factory->createAuth();

    }

    public function getValues($request, $type) {

        if ($type == "date") {
            
            $data = $this->validate($request, [
                'i' => 'required|date',
                'f' => 'required|date|after_or_equal:start_date',
            ]); 

        } else if ($type == "user") {

            $data = $this->validate($request, [
                'user' => 'required|email'
            ]); 

        } else {
            $data = null;
        }
        
        return $data;
    }

    public function validateUserUpdate($request) {
        $data = $this->validate($request, [
            'name' => 'max:255',
            'surname' => 'max:255',
            'email' => 'email',
            'pass' => 'confirmed',
        ]); 

        return $data;
    }


    public function validateLogin($request) {

        $data = $this->validate($request, [
            'email' => 'required|email',
            'pass' => 'required'
        ]);

        return $data;

    }

<<<<<<< HEAD
}
=======
}
>>>>>>> validacao usuario
