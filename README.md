> Rotas de EVENTOS

| 	URL|  Método| 	Dados|  Descrição|
|--|--|--|--|
| ``/eventos`` | GET | -- | Listar todos os eventos |
| ``/eventos/visualizar/{id}`` | GET | **id**:string | Visualiza um único evento |
| ``/eventos/salvar`` | POST | <p>**title**:text required <br> **start_date**:timestamp 'd-m-Y h:i:s' required <br> **end_date**:timestamp 'd-m-Y h:i:s' required <br> **type**:text is('Interna', 'Externa', 'Vídeoconferência') required <br> **location**:text type == 'Externa' : required <br> **description**:text <br> **tagged_emails[]**:array(string) required</p> | Salva um evento |
| ``/eventos/filtrar/?user={email}`` | GET | **user**:email required | Filtra todos eventos pelo e-mail |
| ``/eventos/filtrar?i={datetime}&f={datetime}`` | GET | <p> **i**:datetime 'd-m-Y h:i:s' required <br> **f**:datetime 'd-m-Y h:i:s' required <p> | Filtra todos eventos pela data e horas informados |
| ``/eventos/excluir/{id}`` | DELETE | **id**:string required | Exclui um registro |

> Rotas de EVENTOS

|  URL| Método |Dados	| Descrição |
|--|--|--|--|
| `/usuario/visualizar/{email}` | GET | <b>email</b>:string |Visualiza um único registro
| `/usuario` | GET | -- |Listar todos os registros
| `/usuario/salvar` | POST |<p><b>email</b>:email required<br><b>password</b>:string required<br><b>displayName</b>:string required<br></p>|Salvar registro
| `/usuario/alterar/{email}` | POST | <p><b>email</b>:email<br><b>password</b>:string<br><b>displayName</b>:string<br></p>|Altera um registro de acordo com o email informado|
`/usuario/excluir/{email}` | DELETE | <p><b>email</b>:email<br></p>|Exclui um registro de acordo com o email informado|