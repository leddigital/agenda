<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'UserController@login');

// Rotas dos eventos
$router->group(['prefix' => 'eventos', 'middleware' => 'auth'], function () use ($router) {

    $router->get('/', 'ScheduleController@showAll');
    $router->get('/visualizar/{id}', 'ScheduleController@showSingle');
    $router->get('/filtrar', 'ScheduleController@filter');

    $router->post('/salvar', 'ScheduleController@insert');
    $router->post('/alterar/{id}', 'ScheduleController@update');
    
    $router->delete('/excluir/{id}', 'ScheduleController@delete');

});

// Rotas dos usuários
$router->group(['prefix' => 'usuario', 'middleware' => 'auth'], function () use ($router) {
    
    $router->get('/', 'UserController@showAll');
    $router->get('/visualizar/{email}', 'UserController@showSingle');

    $router->post('/autenticar', 'UserController@authenticate');
    $router->post('/salvar', 'UserController@insert');
    $router->post('/alterar/{email}', 'UserController@update');

    $router->delete('/excluir/{email}', 'UserController@delete');

});

